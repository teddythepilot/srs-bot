﻿// OverlordBot is an AWACS/ATC bot for DCS World
// Copyright (C) 2022 Jeffrey Jones
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY

using NLog;

namespace RurouniJones.OverlordBot.Core.Util
{
    internal class SsmlWrapper
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        public static string Wrap(string text, string voice)
        {
            var response =
                $"<speak version=\"1.0\" xmlns=\"https://www.w3.org/2001/10/synthesis\" xml:lang=\"{voice[..5]}\"><voice name =\"{voice}\">{text}</voice></speak>";
            Logger.Debug($"Transmission SSML response: {response}");
            return response;
        }
    }
}