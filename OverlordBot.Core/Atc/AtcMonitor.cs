﻿// OverlordBot is an AWACS/ATC bot for DCS World
// Copyright (C) 2022 Jeffrey Jones
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Geo.Measure;
using NLog;
using RurouniJones.OverlordBot.Cognitive.LanguageUnderstanding.Transmissions;
using RurouniJones.OverlordBot.Core.Atc.Util;
using RurouniJones.OverlordBot.Core.Monitors;
using RurouniJones.OverlordBot.Core.Util;
using RurouniJones.OverlordBot.Datastore;
using RurouniJones.OverlordBot.Encyclopedia;

namespace RurouniJones.OverlordBot.Core.Atc
{
    public class AtcMonitor : IMonitor
    {
        private static readonly List<ControlledAircraft.State> InboundStates = new()
            {
                ControlledAircraft.State.Inbound, ControlledAircraft.State.Base, ControlledAircraft.State.Final,
                ControlledAircraft.State.ShortFinal
            };

        private readonly HashSet<AirfieldStatus> _airfields;
        private readonly Logger _logger = LogManager.GetCurrentClassLogger();
        private readonly RadioController.QueueTransmissionDelegate _queueTransmission;
        private readonly RadioController.LogTransmissionDelegate _logTransmission;
        private readonly IPlayerRepository _playerRepository;

        public AtcMonitor(HashSet<AirfieldStatus> airfields,
            RadioController.QueueTransmissionDelegate queueTransmissionDelegate,
            RadioController.LogTransmissionDelegate logTransmissionDelegate,
            IPlayerRepository playerRepository)
        {
            _queueTransmission = queueTransmissionDelegate;
            _logTransmission = logTransmissionDelegate;
            _playerRepository = playerRepository;
            _airfields = airfields;
        }

        public async Task StartMonitoring(CancellationToken cancellationToken)
        {
            _logger.Info("Starting ATC monitoring");
            while (!cancellationToken.IsCancellationRequested)
            {
                try { 
                    await Task.Delay(TimeSpan.FromMilliseconds(100), CancellationToken.None);
                    foreach (var playersAtAirfield in _airfields.Select(airfield =>
                        airfield.ControlledPlayers.Values.ToHashSet()))
                    {
                        if (playersAtAirfield.Count == 0) continue;
                        await CheckProgress(SortPlayers(playersAtAirfield));
                    }
                }
                catch (Exception ex)
                {
                    _logger.Warn(ex);
                    continue;
                }
            }
            _logger.Info("Stopped ATC monitoring");
        }

        public async Task ProcessTransmission(ITransmission transmission)
        {
            if (transmission.Intent != ITransmission.Intents.ReadyToTaxi
                && transmission.Intent != ITransmission.Intents.InboundToAirfield
                && transmission.Intent != ITransmission.Intents.AbortInbound ) return;

            var basicTransmission = (BasicTransmission) transmission;
            var transmittingPlayer = (ITransmission.Player) basicTransmission.Transmitter;

            var transmittingUnit = await _playerRepository.FindByCallsign(transmittingPlayer.GroupName,
                transmittingPlayer.Flight,
                transmittingPlayer.Element);

            var receiver = (ITransmission.Airfield) transmission.Receiver;

            var airfield = _airfields.First(x => x.Name.ToLower().Equals(receiver.Name.ToLower()));

            ControlledAircraft player;
            switch (transmission.Intent)
            {
                case ITransmission.Intents.ReadyToTaxi:
                {
                    var wayPoints = TaxiRouter.Process(transmittingUnit.Location, airfield).TaxiPoints;
                    player = new ControlledAircraft(transmittingPlayer, transmittingUnit, airfield, wayPoints,
                        _queueTransmission, _logTransmission);
                    player.CalledTaxi();
                    break;
                }
                case ITransmission.Intents.InboundToAirfield:
                {
                    var approachRoute = airfield.GetApproachRoute(transmittingUnit.Location);

                    var currentPosition = new Airfield.NavigationPoint
                    {
                        Name = "Inbound Position",
                        Latitude = transmittingUnit.Location.Coordinate.Latitude,
                        Longitude = transmittingUnit.Location.Coordinate.Longitude
                    };

                    approachRoute = approachRoute.Prepend(currentPosition).ToList();

                    player = new ControlledAircraft(transmittingPlayer, transmittingUnit, airfield, approachRoute,
                        _queueTransmission, _logTransmission);

                    player.CalledInbound();
                    break;
                }
                default:
                    player = airfield.ControlledPlayers[transmittingUnit.Id];
                    player.AbortInbound();
                    return;
            }

            if (airfield.ControlledPlayers.Keys.Contains(player.PlayerUnit.Id))
            {
                _logger.Debug($"Player {player.PlayerCallsign} already in ATC monitoring. Removed");
                airfield.ControlledPlayers.Remove(player.PlayerUnit.Id, out _);
            }

            _logger.Debug($"Player {player.PlayerCallsign} added to ATC monitoring");
            airfield.ControlledPlayers[player.PlayerUnit.Id] = player;
        }

        /// <summary>
        /// Sort the players based on transmission priority.
        /// </summary>
        /// <param name="players">unsorted list of players active on ATC at this airfield</param>
        /// <returns>Players sorted by priority</returns>
        public List<ControlledAircraft> SortPlayers(HashSet<ControlledAircraft> players)
        {
            try
            {
                var tempPlayers = players.ToList();
                var orderedPlayers = new List<ControlledAircraft>();

                // Check people inbound
                foreach (var player in tempPlayers.ToList()
                    .Where(player => InboundStates.Contains(player.CurrentState)))
                {
                    orderedPlayers.Add(player);
                    tempPlayers.Remove(player);
                }

                // Check people lined up and waiting
                foreach (var player in tempPlayers.ToList().Where(player =>
                    player.CurrentState == ControlledAircraft.State.LinedUpAndWaiting))
                {
                    orderedPlayers.Add(player);
                    tempPlayers.Remove(player);
                }

                // Check people holding short in order of distance from the runway
                var holdingShortPlayers = tempPlayers.ToList()
                    .Where(player => player.CurrentState == ControlledAircraft.State.HoldingShort)
                    .OrderBy(player => player.DistanceTo(player.CurrentNavigationPoint, DistanceUnit.M))
                    .ToList();

                foreach (var player in holdingShortPlayers)
                {
                    orderedPlayers.Add(player);
                    tempPlayers.Remove(player);
                }

                var otherPlayers = tempPlayers.ToList()
                    .OrderBy(player => player.DistanceTo(player.CurrentNavigationPoint, DistanceUnit.M))
                    .ToList();

                // Then the rest based on distance to their next navigation point. This makes sure we always talk to the first in line for a given point
                foreach (var player in otherPlayers)
                {
                    orderedPlayers.Add(player);
                    tempPlayers.Remove(player);
                }

                return orderedPlayers;
            }
            catch (Exception ex)
            {
                _logger.Warn(ex);
                return new List<ControlledAircraft>();
            }
        }

        private async Task CheckProgress(IEnumerable<ControlledAircraft> orderedPlayers)
        {
            // Now check them in that order
            foreach (var player in orderedPlayers)
            {
                var playerUnit = await _playerRepository.FindByCallsign(
                    player.PlayerCallsign.GroupName,
                    player.PlayerCallsign.Flight,
                    player.PlayerCallsign.Element
                    );

                await player.CheckProgress(playerUnit);

            }
        }
    }
}