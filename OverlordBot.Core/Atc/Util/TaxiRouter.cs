﻿// OverlordBot is an AWACS/ATC bot for DCS World
// Copyright (C) 2022 Jeffrey Jones
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY

using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using Geo.Geometries;
using NLog;
using QuikGraph;
using QuikGraph.Algorithms;
using RurouniJones.OverlordBot.Core.Atc.Extensions;
using RurouniJones.OverlordBot.Encyclopedia;

[assembly: InternalsVisibleTo("OverlordBot.Core.Tests.AtcResponder")]

namespace RurouniJones.OverlordBot.Core.Atc.Util
{
    internal class TaxiRouter
    {
        protected static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        public static TaxiInstructions Process(Point callerPosition, AirfieldStatus airfield)
        {
            Airfield.TaxiPoint source;
            try
            {
                source = airfield.TaxiPoints.OrderBy(taxiPoint => taxiPoint.DistanceTo(callerPosition.Coordinate))
                    .First();
            }
            catch (NullReferenceException)
            {
                throw new TaxiPathNotFoundException("Taxi path not available because no TaxiPoints found");
            }

            Logger.Debug($"Player is at {callerPosition.Coordinate}, nearest Taxi point is {source}");
            var runways = ActiveRunwayDecider.GetActiveRunways(airfield);
            return runways.Count == 1
                ? GetTaxiInstructionsWhenSingleRunway(source, runways.First(), airfield)
                : GetTaxiInstructionsWhenMultipleRunways(source, runways, airfield);
        }

        private static TaxiInstructions GetTaxiInstructionsWhenMultipleRunways(Airfield.NavigationPoint source,
            IEnumerable<Airfield.Runway> runways, AirfieldStatus airfield)
        {
            var tryGetPaths = airfield.NavigationGraph.ShortestPathsDijkstra(airfield.NavigationCostFunction, source);

            var cheapestPathCost = double.PositiveInfinity;
            IEnumerable<TaggedEdge<Airfield.NavigationPoint, string>> cheapestPath =
                new List<TaggedEdge<Airfield.NavigationPoint, string>>();
            Airfield.Runway closestRunway = null;

            foreach (var runway in runways)
            {
                if (!tryGetPaths(runway, out var path)) continue;
                var taggedEdges = path.ToList();
                var pathCost = PathCost(taggedEdges, airfield);
                if (!(pathCost < cheapestPathCost)) continue;
                closestRunway = runway;
                cheapestPath = taggedEdges;
                cheapestPathCost = pathCost;
            }

            return CompileInstructions(closestRunway, cheapestPath);
        }

        private static TaxiInstructions GetTaxiInstructionsWhenSingleRunway(Airfield.NavigationPoint source,
            Airfield.NavigationPoint target, AirfieldStatus airfield)
        {
            var tryGetPaths = airfield.NavigationGraph.ShortestPathsDijkstra(airfield.NavigationCostFunction, source);
            if (tryGetPaths(target, out var path)) return CompileInstructions(target, path);
            throw new TaxiPathNotFoundException($"No taxi path found from {source.Name} to {target.Name}");
        }

        private static TaxiInstructions CompileInstructions(Airfield.NavigationPoint target,
            IEnumerable<TaggedEdge<Airfield.NavigationPoint, string>> path)
        {
            var taggedEdges = path.ToList();

            var taxiInstructions = new TaxiInstructions
            {
                DestinationName = target.Name,
                TaxiPoints = taggedEdges.Select(edge => edge.Source).ToList(),
                Comments = new List<string>()
            };

            // Include the final NavigationPoint
            taxiInstructions.TaxiPoints.Add(taggedEdges.Last().Target);
            foreach (var edge in taggedEdges) taxiInstructions.TaxiwayNames.Add(edge.Tag);

            taxiInstructions.TaxiwayNames = RemoveRepeating(taxiInstructions.TaxiwayNames);

            Logger.Debug(taxiInstructions);

            return taxiInstructions;
        }

        private static List<string> RemoveRepeating(IReadOnlyList<string> taxiways)
        {
            var dedupedTaxiways = new List<string>();

            for (var i = 0; i < taxiways.Count; i++)
                if (i == 0)
                    dedupedTaxiways.Add(taxiways[0]);
                else if (taxiways[i] != taxiways[i - 1]) dedupedTaxiways.Add(taxiways[i]);
            return dedupedTaxiways;
        }

        private static double PathCost(IEnumerable<TaggedEdge<Airfield.NavigationPoint, string>> path,
            AirfieldStatus airfield)
        {
            return path.Sum(edge => airfield.NavigationCost[edge]);
        }

        public class TaxiInstructions
        {
            public string DestinationName { get; set; }

            public List<Airfield.NavigationPoint> TaxiPoints { get; set; }

            public List<string> TaxiwayNames { get; set; } = new();

            public List<string> Comments { get; set; } = new();

            public override string ToString()
            {
                var val = $"{DestinationName}, {string.Join("-", TaxiwayNames)}";
                if (Comments.Count > 0) val += $" , {string.Join(" ", Comments)} ";
                return val;
            }
        }

        public class TaxiPathNotFoundException : Exception
        {
            public TaxiPathNotFoundException(string message) : base(message)
            {
            }
        }
    }
}