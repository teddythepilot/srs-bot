﻿// OverlordBot is an AWACS/ATC bot for DCS World
// Copyright (C) 2022 Jeffrey Jones
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY

using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using Geo.Geometries;
using NLog;
using QuikGraph;
using QuikGraph.Algorithms;
using RurouniJones.OverlordBot.Core.Atc;
using RurouniJones.OverlordBot.Core.Atc.Extensions;
using RurouniJones.OverlordBot.Core.Atc.Util;
using RurouniJones.OverlordBot.Encyclopedia;

[assembly: InternalsVisibleTo("OverlordBot.Core.Tests")]
namespace RurouniJones.OverlordBot.Core
{
    public class AirfieldStatus
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        private readonly Airfield _airfield;

        /// <summary>
        ///     A lister of players currently being monitored at this airfield
        /// </summary>
        public readonly ConcurrentDictionary<string, ControlledAircraft> ControlledPlayers =
            new();

        public readonly string ServerShortName;

        /// <summary>
        ///     See QuikGraph for more information on Tagged edges
        /// </summary>
        public Dictionary<TaggedEdge<Airfield.NavigationPoint, string>, double> NavigationCost;

        /// <summary>
        ///     See QuikGraph for more information on navigation costs
        /// </summary>
        public Func<TaggedEdge<Airfield.NavigationPoint, string>, double> NavigationCostFunction;

        /// <summary>
        ///     See QuikGraph for more information on graph types
        /// </summary>
        public AdjacencyGraph<Airfield.NavigationPoint, TaggedEdge<Airfield.NavigationPoint, string>> NavigationGraph =
            new();


        public AirfieldStatus(Airfield airfield, string serverShortName)
        {
            _airfield = airfield;
            ServerShortName = serverShortName;
            BuildTaxiGraph();
        }

        public string Name => _airfield.Name;

        public double Elevation => _airfield.Altitude;

        public HashSet<Airfield.Runway> Runways => _airfield.Runways;

        /// <summary>
        ///     Direction the wind is COMING from
        /// </summary>
        public int WindHeading { get; set; } = 90;

        /// <summary>
        ///     Wind speed 33ft (the standard measurement point) above the airfield in meters / second.
        /// </summary>
        public double WindSpeed { get; set; } = 0;

        /// <summary>
        ///     The coalition the airbase belongs to.
        /// </summary>
        public int Coalition { get; set; } = 0;

        /// <summary>
        ///     A list of all the nodes that make up a runway
        /// </summary>
        public Dictionary<Airfield.Runway, List<Airfield.NavigationPoint>> RunwayNodes { get; set; } =
            new();

        public IEnumerable<Airfield.TaxiPoint> TaxiPoints => NavigationGraph.Vertices.OfType<Airfield.TaxiPoint>();

        private void BuildTaxiGraph()
        {
            try
            {
                foreach (var runway in _airfield.Runways) NavigationGraph.AddVertex(runway);
                foreach (var parkingSpot in _airfield.ParkingSpots) NavigationGraph.AddVertex(parkingSpot);
                foreach (var junction in _airfield.Junctions) NavigationGraph.AddVertex(junction);
                foreach (var wayPoint in _airfield.WayPoints) NavigationGraph.AddVertex(wayPoint);

                NavigationCost =
                    new Dictionary<TaggedEdge<Airfield.NavigationPoint, string>, double>(NavigationGraph.EdgeCount);

                foreach (var taxiway in _airfield.NavigationPaths)
                {
                    TaggedEdge<Airfield.NavigationPoint, string> edge;
                    try
                    {
                        var source = NavigationGraph.Vertices.First(taxiPoint => taxiPoint.Name.Equals(taxiway.Source));
                        var target = NavigationGraph.Vertices.First(taxiPoint => taxiPoint.Name.Equals(taxiway.Target));
                        var tag = taxiway.Name;

                        edge = new TaggedEdge<Airfield.NavigationPoint, string>(source, target, tag);
                    }
                    catch (InvalidOperationException e)
                    {
                        Logger.Error(e, $"Could not create Tagged Edge for {_airfield.Name}. Edge source: {taxiway.Source}, Edge target: {taxiway.Target}.");
                        continue;
                    }
                    NavigationGraph.AddEdge(edge);

                    NavigationCost.Add(edge, taxiway.Cost);
                }

                NavigationCostFunction = AlgorithmExtensions.GetIndexer(NavigationCost);
                Logger.Debug($"{_airfield.Name} airfield navigation graph built");
            }
            catch (Exception e)
            {
                Logger.Error(e, $"Could not build navigation graph for {_airfield.Name}");
            }

            // Now populate the runway nodes so we know all the nodes that make up a runway
            try
            {
                foreach (var runway in _airfield.Runways)
                {
                    Airfield.NavigationPoint node = runway;
                    var nodes = new List<Airfield.NavigationPoint> {node};
                    while (true)
                    {
                        // ReSharper disable once AccessToModifiedClosure
                        var edges = NavigationGraph.Edges.Where(x => x.Source == node);
                        try
                        {
                            node = edges.First(x => x.Tag == "Runway" && !nodes.Contains(x.Target)).Target;
                            nodes.Add(node);
                            if (node is Airfield.Runway) break;
                        }
                        catch (InvalidOperationException)
                        {
                            break;
                        }
                    }

                    RunwayNodes.Add(runway, nodes);
                    Logger.Trace($"{_airfield.Name} {runway} nodes built");
                }
            }
            catch (Exception e)
            {
                Logger.Error(e, $"Could not build runway nodes for {_airfield.Name}");
            }
        }

        public List<Airfield.NavigationPoint> GetApproachRoute(Point callerPosition)
        {
            var paths = GetPathsToActiveRunways();
            var taggedEdges = paths.OrderBy(x => x[0].Source.DistanceTo(callerPosition.Coordinate)).First();
            var navigationPoints = taggedEdges.Select(edge => edge.Source).ToList();
            // Add the runway itself
            navigationPoints.Add(taggedEdges.Last().Target);
            return navigationPoints;
        }

        private IEnumerable<List<TaggedEdge<Airfield.NavigationPoint, string>>> GetPathsToActiveRunways()
        {
            // First we get a list of active runway(s)
            var activeRunways = ActiveRunwayDecider.GetActiveRunways(this);

            // Then we get a list of all the entry Waypoints
            var entryPoints = _airfield.WayPoints.Where(x => x.Name.ToLower().Contains("entry"));

            var paths = new List<KeyValuePair<double, List<TaggedEdge<Airfield.NavigationPoint,string>>>>();

            // Get all the paths from all the entry waypoints to the active runway(s)
            foreach (var entryPoint in entryPoints)
            {
                var tryGetPaths =
                    NavigationGraph.ShortestPathsDijkstra(NavigationCostFunction, entryPoint);

                foreach (var runway in activeRunways)
                {
                    if (!tryGetPaths(runway, out var path)) continue;
                    var taggedEdges = path.ToList();
                    var pathCost = PathCost(taggedEdges);
                    var pathWithCost = new KeyValuePair<double, List<TaggedEdge<Airfield.NavigationPoint,string>>>(pathCost, taggedEdges);
                    paths.Add(pathWithCost);
                }
            }

            // Sort so that we get the cheapest ones first
            var cheapestPathsByCost = paths.OrderBy(pair => pair.Key).ToList();

            // This is the cheapest one, so we want this one and any that have the same cost
            // since there could be multiple entry points for a given active runway(s).
            var cheapestCost = cheapestPathsByCost.First().Key;

            cheapestPathsByCost.RemoveAll(pair => pair.Key > cheapestCost);

            // And finally return the paths so the caller can find the one with the closest
            // start point to the player, which will be the actual approach path.
            return cheapestPathsByCost.Select(pathPair => pathPair.Value).ToList();
        }

        protected double PathCost(IEnumerable<TaggedEdge<Airfield.NavigationPoint, string>> path)
        {
            return path.Sum(edge => NavigationCost[edge]);
        }
    }
}