﻿// OverlordBot is an AWACS/ATC bot for DCS World
// Copyright (C) 2022 Jeffrey Jones
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY

using System;
using System.Linq;
using Newtonsoft.Json.Linq;
using RurouniJones.OverlordBot.Cognitive.LanguageUnderstanding.Transmissions;

namespace RurouniJones.OverlordBot.Cognitive.LanguageUnderstanding
{
    public class IntentDetails
    {
        public ITransmission.Intents Intent;
        public ITransmission.ITransmitter Transmitter;
        public ITransmission.IReceiver Receiver;
        public ITransmission.ISubject Subject;
        public ITransmission.MissionType MissionType;
        public int MissionId;


        public IntentDetails(ITransmission.Intents intent, JToken intentEntity)
        {
            Intent = intent;

            var player = SetPlayerCallsign(intentEntity.SelectToken("transmitter[0]"));
            Transmitter = player != null ? player : new ITransmission.UnknownEntity();

            Receiver = SetReceiver(intentEntity);
            Subject = SetSubject(intentEntity.SelectToken("subject[0]"));

            var missionType = intentEntity.SelectToken("missionType[0][0]")?.ToString();
            var _ = int.TryParse(intentEntity.SelectToken("missionId[0]")?.ToString(), out MissionId);

            if (missionType == null)
            {
                MissionType = ITransmission.MissionType.Unknown;
            }
            else
            {
                try
                {
                    MissionType = (ITransmission.MissionType) Enum.Parse(typeof(ITransmission.MissionType), missionType, true);
                }
                catch
                {
                    MissionType = ITransmission.MissionType.Unknown;
                }
            }
        }

        private static ITransmission.Player SetPlayerCallsign(JToken entity)
        {
            if (entity == null) return null;

            var groupName = entity.SelectToken("player[0].groupName[0]")?.ToString();
            var flight = MapToInt(entity.SelectToken("player[0].flight[0]")?.ToString());
            var element = MapToInt(entity.SelectToken("player[0].element[0]")?.ToString());
            var flightAndElement = entity.SelectToken("player[0].flightAndElement[0]")?.ToString();

            // The speech recognition is not good enough to always hear two word callsigns
            // (such as "GhostRider") as one word despite training. So we train the language instead
            // to accept two words as callsigns (e.g. "Ghost rider") and then strip the space here.
            // Callsigns cannot have spaces to that should work and make the bot more reliable.
            if (groupName != null)
            {
                groupName = string.Concat(groupName.Where(c => !char.IsWhiteSpace(c)));
            }

            // ReSharper disable once InvertIf
            if (flightAndElement != null)
            {
                flight = MapToInt(flightAndElement[0].ToString());
                element = MapToInt(flightAndElement[1..]);
            }

            return new ITransmission.Player(EntityAliasResolver.ResolveGroupName(groupName), flight, element);
        }

        private static ITransmission.IReceiver SetReceiver(JToken entity)
        {
            var awacsCallsign = entity.SelectToken("receiver[0].awacs[0]")?.ToString();
            if(awacsCallsign != null) return new ITransmission.Awacs(awacsCallsign);

            var airfield = entity.SelectToken("receiver[0].airfield[0]");
            if (airfield == null) return new ITransmission.UnknownEntity();

            var token = airfield.SelectToken("name[0]");
            var name = token is JArray ? token.First?.ToString() : token?.ToString();
            var controller = airfield.SelectToken("controller[0]")?.ToString();
            return new ITransmission.Airfield(name, controller);

        }

        private static ITransmission.ISubject SetSubject(JToken subject)
        {
            if (subject == null) return null; 

            if (subject["player"] != null) return SetPlayerCallsign(subject);
            if (subject["tanker"] != null) return new ITransmission.Tanker(subject["tanker"].First?.ToString());
            return subject["airfield"]!= null ? new ITransmission.Airfield(subject["airfield"].First?.First?.ToString(), null) : null;
        }

        private static int MapToInt(string value)
        {
            if (value == null) return -1;
            if(int.TryParse(value, out var number))
            {
                return number;
            }
            var lowerValue = value.ToLower();
            return lowerValue switch
            {
                "zero" => 0,
                "one" => 1,
                "two" => 2,
                "three" => 3,
                "four" => 4,
                "five" => 5,
                "six" => 6,
                "seven" => 7,
                "eight" => 8,
                "nine" => 9,
                _ => -1
            };
        }
    }
}