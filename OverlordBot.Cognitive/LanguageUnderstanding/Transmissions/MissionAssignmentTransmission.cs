﻿// OverlordBot is an AWACS/ATC bot for DCS World
// Copyright (C) 2022 Jeffrey Jones
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY

using System;
using System.Text;

namespace RurouniJones.OverlordBot.Cognitive.LanguageUnderstanding.Transmissions
{

    public class MissionAssignmentTransmission : ITransmission
    {
        public string Text { get; }
        public ITransmission.Intents Intent  { get; }
        public ITransmission.ITransmitter Transmitter  { get; }
        public ITransmission.IReceiver Receiver { get; }
        public ITransmission.MissionType MissionType { get; }
        public DateTime ReceivedAt { get; }

        public MissionAssignmentTransmission(string request, ITransmission.Intents intent, ITransmission.ITransmitter transmitter, ITransmission.IReceiver receiver, ITransmission.MissionType missionType)
        {
            Text = request;
            Intent = intent;
            Transmitter = transmitter;
            Receiver = receiver;
            MissionType = missionType;
            ReceivedAt = DateTime.Now;
        }

        public string ToDiscordLog()
        {
            var transmitterType = "";
            var groupName = "_Unknown_";
            var flight = "_Unknown_";
            var element = "_Unknown_";

            var receiverType = "_Unknown";
            var receiverCallsign = "_Unknown_";

            if (Receiver != null)
            {
                receiverType = Receiver.GetType().Name;
                receiverCallsign = Receiver.Callsign;
            }

            if (Transmitter is ITransmission.Player transmitter)
            {
                transmitterType = transmitter.GetType().Name;
                if (transmitter.GroupName != null) groupName = transmitter.GroupName;
                if (transmitter.Flight >= 0) flight = transmitter.Flight.ToString();
                if (transmitter.Element >= 0) element = transmitter.Element.ToString();
            }


            var sb = new StringBuilder()
                .AppendLine("Request:")
                .AppendLine($"\tText: {Text}")
                .AppendLine($"\tTransmitter: {transmitterType}")
                .AppendLine($"\t\tGroup Name: {groupName}")
                .AppendLine($"\t\tFlight Number: {flight}")
                .AppendLine($"\t\tElement Number: {element}")
                .AppendLine($"\tReceiver: {Receiver?.GetType().Name ?? "_Unknown_"}")
                .AppendLine($"\t\tCallsign: {Receiver?.Callsign ?? "_Unknown_"}")
                .AppendLine($"\tIntent: {Intent}")
                .AppendLine($"\tMission: {MissionType}");

            return sb.ToString();
        }
    }
}
