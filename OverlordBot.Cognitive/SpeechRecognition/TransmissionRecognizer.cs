﻿// OverlordBot is an AWACS/ATC bot for DCS World
// Copyright (C) 2022 Jeffrey Jones
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY

using System;
using System.Threading.Tasks;
using Microsoft.CognitiveServices.Speech;
using Microsoft.CognitiveServices.Speech.Audio;
using NAudio.Wave;
using NLog;
using RurouniJones.OverlordBot.Cognitive.SpeechRecognition.Util;

namespace RurouniJones.OverlordBot.Cognitive.SpeechRecognition
{
    public class TransmissionRecognizer
    {
        public class SpeechServiceException : Exception
        {
            public SpeechServiceException(string message) : base(message) {}
        }

        private readonly BufferedWaveProvider _audioBuffer;
        private readonly Logger _logger = LogManager.GetCurrentClassLogger();

        public static SpeechConfig SpeechConfig { private get; set; }

        private readonly SpeechRecognizer _speechRecognizer;

        public TransmissionRecognizer(BufferedWaveProvider audioBuffer, string clientName)
        {
            _audioBuffer = audioBuffer;
            _logger.SetProperty("Controller", clientName);

            var streamReader = new BufferedWaveProviderStreamReader(audioBuffer);
            var audioConfig = AudioConfig.FromStreamInput(streamReader, AudioStreamFormat.GetWaveFormatPCM(16000, 16, 1));

            _speechRecognizer = new SpeechRecognizer(SpeechConfig, audioConfig);
        }

        public async Task<Result> RecognizeTransmissionAsync()
        {
            _logger.Debug("Starting Transmission Recognition");
            var recognitionResult = await _speechRecognizer.RecognizeOnceAsync();
            _audioBuffer.ClearBuffer();
            var result = new Result(recognitionResult);
            if (result.IsSuccess)
            {
                _logger.Info($"Recognition succeeded: \"{result.Text}\"");
            }
            else
            {
                var cancellation = CancellationDetails.FromResult(recognitionResult);
                var message = $"Recognition failed: {cancellation.Reason} due to {cancellation.ErrorCode}, Details: {cancellation.ErrorDetails}";
                _logger.Warn(message);

                if (cancellation.ErrorCode == CancellationErrorCode.ServiceTimeout ||
                    cancellation.ErrorCode == CancellationErrorCode.ServiceError ||
                    cancellation.ErrorCode == CancellationErrorCode.ServiceUnavailable)
                {
                    throw new SpeechServiceException(cancellation.ErrorDetails);
                }
            }
            return result;
        }

        public class Result
        {
            public string Text { get; }

            public bool IsSuccess { get; }

            public Result(RecognitionResult speechRecognitionResult)
            {
                Text = speechRecognitionResult.Text;
                IsSuccess = speechRecognitionResult.Reason == ResultReason.RecognizedSpeech;
            }
        }
    }
}
