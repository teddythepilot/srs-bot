﻿// OverlordBot is an AWACS/ATC bot for DCS World
// Copyright (C) 2022 Jeffrey Jones
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY

using System;
using System.Collections.Generic;
using System.IO;
using Concentus.Enums;
using Concentus.Structs;
using NAudio.Wave;
using NLog;

namespace RurouniJones.OverlordBot.SimpleRadio.Audio
{
    public class NetworkAudioEncoder
    {
        private const int InputSampleRate = 16000;
        private const int Channels = 1; // Mono audio
        private const int Bits = 16;
        private const int InputAudioLength = 40; // ms
        private const int FrameSize = InputSampleRate / 1000 * InputAudioLength;

        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        public static Queue<(byte[], TimeSpan)> Encode(byte[] pcmAudio)
        {
            var waveStream = WaveFormatConversionStream.CreatePcmStream(
                new RawSourceWaveStream(
                    new MemoryStream(pcmAudio),
                    new WaveFormat(InputSampleRate, Bits, Channels)));

            Logger.Debug("Spoken time is " + waveStream.TotalTime.Seconds + "." + waveStream.TotalTime.Milliseconds + " seconds");

            var encoder = new OpusEncoder(InputSampleRate, Channels, OpusApplication.OPUS_APPLICATION_VOIP);

            var opusPacketsWithTimestamps = new Queue<(byte[], TimeSpan)>();
            var pcmBuffer = new byte[FrameSize * 2];

            while (waveStream.Read(pcmBuffer, 0, pcmBuffer.Length) > 0)
            {
                var pcm = ByteArrayToShortArray(pcmBuffer);

                var dataPacket = new byte[FrameSize];

                encoder.Encode(pcm, 0, FrameSize, dataPacket, 0, FrameSize);

                var tuple = (Packet: dataPacket, Timestamp: waveStream.CurrentTime);

                opusPacketsWithTimestamps.Enqueue(tuple);
            }

            return opusPacketsWithTimestamps;
        }

        private static short[] ByteArrayToShortArray(byte[] data)
        {
            var shortArray = new short[data.Length / sizeof(short)];
            Buffer.BlockCopy(data, 0, shortArray, 0, data.Length);
            return shortArray;
        }
    }
}
