﻿// OverlordBot is an AWACS/ATC bot for DCS World
// Copyright (C) 2022 Jeffrey Jones
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY

using System.Collections.Generic;
using Newtonsoft.Json;
using RurouniJones.OverlordBot.SimpleRadio.Models;

namespace RurouniJones.OverlordBot.SimpleRadio.Network.Util
{
    public class NetworkMessage
    {
        public enum MessageType
        {
            Update, //META Data update - No Radio Information
            Ping,
            Sync,
            RadioUpdate, //Only received server side
            ServerSettings,
            ClientDisconnect, // Client disconnected
            VersionMismatch,
            ExternalAwacsModePassword, // Received server side to "authenticate"/pick side for external AWACS mode
            ExternalAwacsModeDisconnect // Received server side on "voluntary" disconnect by the client (without closing the server connection)
        }

        private static readonly JsonSerializerSettings JsonSerializerSettings = new()
        {
            NullValueHandling = NullValueHandling.Ignore // same some network bandwidth
        };

        [JsonProperty(PropertyName = "Clients")]
        public List<ClientInformation> Clients { get; set; }

        [JsonProperty(PropertyName = "Client")]
        public ClientInformation ClientInformation { get; set; }

        public MessageType MsgType { get; set; }

        public string ExternalAwacsModePassword { get; set; }

        public string Version { get; set; }

        public string Encode()
        {
            Version = Client.Version;
            return JsonConvert.SerializeObject(this, JsonSerializerSettings) + "\n";
        }
    }
}