﻿// OverlordBot is an AWACS/ATC bot for DCS World
// Copyright (C) 2022 Jeffrey Jones
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY

using System.Collections.Generic;
using System.Threading.Tasks;
using Geo.Geometries;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using RurouniJones.OverlordBot.Cognitive.LanguageUnderstanding.Transmissions;
using RurouniJones.OverlordBot.Core.Tests.Mocks;
using RurouniJones.OverlordBot.Datastore.Models;

namespace RurouniJones.OverlordBot.Core.Tests.AwacsResponder.Intents
{
    /// <summary>
    /// Verify the bot responds correctly to bogey dope requests.
    /// </summary>
    [TestClass]
    public class BogeyDopeTests
    {
        [TestMethod]
        public async Task WhenNoEnemyContacts_ThenReturnsCorrectResponse()
        {
            var playerRepository = new MockPlayerRepository
            {
                FindByCallSignResult = new Unit
                {
                    Location = new Point(0, 0)
                }
            };

            var controller = new Awacs.AwacsResponder(playerRepository, new MockUnitRepository(), new MockAirfieldRepository(), false, null, null, "TEST");

            var player = new ITransmission.Player("dolt", 1, 2);
            var awacs = new ITransmission.Awacs("Overlord");
            var qualifier = new ITransmission.Aircraft(null, null);
            var transmission = new BogeyDopeTransmission(null, ITransmission.Intents.BogeyDope, player, awacs, qualifier);

            const string expectedResponse = "dolt 1 2, overlord, Nothing on Scope";
            var actualResponse = await controller.ProcessTransmission(transmission);

            Assert.AreEqual(expectedResponse, actualResponse.ToSpeech());
        }

        #region Imperial / Metric tests

        [TestMethod]
        public async Task WhenTransmitterIsImperial_ThenReturnsCorrectResponse()
        {
            var playerRepository = new MockPlayerRepository
            {
                FindByCallSignResult = new Unit
                {
                    Location = new Point(41, 37),
                    Name = "F-15C"
                }
            };

            var unitRepository = new MockUnitRepository
            {
                FindHostileAircraftResult = new List<Unit>
                {
                    new()
                    {
                        Location = new Point(41.5, 37),
                        Name = "MiG-21Bis",
                        Altitude = 1450
                    }
                }
            };

            var controller = new Awacs.AwacsResponder(playerRepository, unitRepository, new MockAirfieldRepository(), false, null, null, "TEST");

            var player = new ITransmission.Player("dolt", 1, 2);
            var awacs = new ITransmission.Awacs("Overlord");
            var qualifier = new ITransmission.Aircraft(null, null);
            var transmission = new BogeyDopeTransmission(null, ITransmission.Intents.BogeyDope, player, awacs, qualifier);

            const string expectedResponse = "dolt 1 2, overlord, bra, 3 5 3, 29, five thousand, DRAG NORTH, Fishbed";
            var actualResponse = await controller.ProcessTransmission(transmission);

            Assert.AreEqual(expectedResponse, actualResponse.ToSpeech());
        }

        [TestMethod]
        public async Task WhenTransmitterIsImperial_AndContactAltitudeInFeetIsAboveHighCommentInMeters_ThenReturnsCorrectResponse()
        {
            var playerRepository = new MockPlayerRepository
            {
                FindByCallSignResult = new Unit
                {
                    Location = new Point(41, 37),
                    Name = "F-15C"
                }
            };

            var unitRepository = new MockUnitRepository
            {
                FindHostileAircraftResult = new List<Unit>
                {
                    new()
                    {
                        Location = new Point(41.5, 37),
                        Name = "MiG-21Bis",
                        Altitude = 12000
                    }
                }
            };

            var controller = new Awacs.AwacsResponder(playerRepository, unitRepository, new MockAirfieldRepository(), false, null, null, "TEST");

            var player = new ITransmission.Player("dolt", 1, 2);
            var awacs = new ITransmission.Awacs("Overlord");
            var qualifier = new ITransmission.Aircraft(null, null);
            var transmission = new BogeyDopeTransmission(null, ITransmission.Intents.BogeyDope, player, awacs, qualifier);

            const string expectedResponse = "dolt 1 2, overlord, bra, 3 5 3, 29, thirty-nine thousand, DRAG NORTH, Fishbed";
            var actualResponse = await controller.ProcessTransmission(transmission);

            Assert.AreEqual(expectedResponse, actualResponse.ToSpeech());
        }

        [TestMethod]
        public async Task WhenTransmitterIsMetric_ThenReturnsCorrectResponse()
        {
            var playerRepository = new MockPlayerRepository
            {
                FindByCallSignResult = new Unit
                {
                    Location = new Point(41, 37),
                    Name = "MiG-21Bis"
                }
            };

            var unitRepository = new MockUnitRepository
            {
                FindHostileAircraftResult = new List<Unit>
                {
                    new()
                    {
                        Location = new Point(41.5, 37),
                        Name = "MiG-21Bis",
                        Altitude = 1450
                    }
                }
            };

            var controller = new Awacs.AwacsResponder(playerRepository, unitRepository, new MockAirfieldRepository(), false, null, null, "TEST");

            var player = new ITransmission.Player("dolt", 1, 2);
            var awacs = new ITransmission.Awacs("Overlord");
            var qualifier = new ITransmission.Aircraft(null, null);
            var transmission = new BogeyDopeTransmission(null, ITransmission.Intents.BogeyDope, player, awacs, qualifier);

            const string expectedResponse = "dolt 1 2, overlord, bra, 3 5 3, 55, one thousand four hundred, DRAG NORTH, Fishbed";
            var actualResponse = await controller.ProcessTransmission(transmission);

            Assert.AreEqual(expectedResponse, actualResponse.ToSpeech());
        }

        #endregion

        #region Additional comments tests

        [TestMethod]
        public async Task WhenContactIsHigh_AndTransmitterIsImperial_ThenReturnsCorrectResponse()
        {
            var playerRepository = new MockPlayerRepository
            {
                FindByCallSignResult = new Unit
                {
                    Location = new Point(41, 37),
                    Name = "F-15C"
                }
            };

            var unitRepository = new MockUnitRepository
            {
                FindHostileAircraftResult = new List<Unit>
                {
                    new()
                    {
                        Location = new Point(41.5, 37),
                        Name = "MiG-21Bis",
                        Altitude = 15000
                    }
                }
            };

            var controller = new Awacs.AwacsResponder(playerRepository, unitRepository, new MockAirfieldRepository(), false, null, null, "TEST");

            var player = new ITransmission.Player("dolt", 1, 2);
            var awacs = new ITransmission.Awacs("Overlord");
            var qualifier = new ITransmission.Aircraft(null, null);
            var transmission = new BogeyDopeTransmission(null, ITransmission.Intents.BogeyDope, player, awacs, qualifier);

            const string expectedResponse = "dolt 1 2, overlord, bra, 3 5 3, 29, forty-nine thousand, DRAG NORTH, high, Fishbed";
            var actualResponse = await controller.ProcessTransmission(transmission);

            Assert.AreEqual(expectedResponse, actualResponse.ToSpeech());
        }

        [TestMethod]
        public async Task WhenContactIsHigh_AndTransmitterIsMetric_ThenReturnsCorrectResponse()
        {
            var playerRepository = new MockPlayerRepository
            {
                FindByCallSignResult = new Unit
                {
                    Location = new Point(41, 37),
                    Name = "MiG-21Bis"
                }
            };

            var unitRepository = new MockUnitRepository
            {
                FindHostileAircraftResult = new List<Unit>
                {
                    new()
                    {
                        Location = new Point(41.5, 37),
                        Name = "MiG-21Bis",
                        Altitude = 15000
                    }
                }
            };

            var controller = new Awacs.AwacsResponder(playerRepository, unitRepository, new MockAirfieldRepository(), false, null, null, "TEST");

            var player = new ITransmission.Player("dolt", 1, 2);
            var awacs = new ITransmission.Awacs("Overlord");
            var qualifier = new ITransmission.Aircraft(null, null);
            var transmission = new BogeyDopeTransmission(null, ITransmission.Intents.BogeyDope, player, awacs, qualifier);

            const string expectedResponse = "dolt 1 2, overlord, bra, 3 5 3, 55, fifteen thousand, DRAG NORTH, high, Fishbed";
            var actualResponse = await controller.ProcessTransmission(transmission);

            Assert.AreEqual(expectedResponse, actualResponse.ToSpeech());
        }

        [TestMethod]
        public async Task WhenContactIsVeryFast_ThenReturnsCorrectResponse()
        {
            var playerRepository = new MockPlayerRepository
            {
                FindByCallSignResult = new Unit
                {
                    Location = new Point(41, 37),
                    Name = "F-15C"
                }
            };

            var unitRepository = new MockUnitRepository
            {
                FindHostileAircraftResult = new List<Unit>
                {
                    new()
                    {
                        Location = new Point(41.5, 37),
                        Name = "MiG-21Bis",
                        Altitude = 1500,
                        Speed = 500
                    }
                }
            };

            var controller = new Awacs.AwacsResponder(playerRepository, unitRepository, new MockAirfieldRepository(), false, null, null, "TEST");

            var player = new ITransmission.Player("dolt", 1, 2);
            var awacs = new ITransmission.Awacs("Overlord");
            var qualifier = new ITransmission.Aircraft(null, null);
            var transmission = new BogeyDopeTransmission(null, ITransmission.Intents.BogeyDope, player, awacs, qualifier);

            const string expectedResponse = "dolt 1 2, overlord, bra, 3 5 3, 29, five thousand, DRAG NORTH, very fast, Fishbed";
            var actualResponse = await controller.ProcessTransmission(transmission);

            Assert.AreEqual(expectedResponse, actualResponse.ToSpeech());
        }

        [TestMethod]
        public async Task WhenContactIsFast_ThenReturnsCorrectResponse()
        {
            var playerRepository = new MockPlayerRepository
            {
                FindByCallSignResult = new Unit
                {
                    Location = new Point(41, 37),
                    Name = "F-15C"
                }
            };

            var unitRepository = new MockUnitRepository
            {
                FindHostileAircraftResult = new List<Unit>
                {
                    new()
                    {
                        Location = new Point(41.5, 37),
                        Name = "MiG-21Bis",
                        Altitude = 1500,
                        Speed = 400
                    }
                }
            };

            var controller = new Awacs.AwacsResponder(playerRepository, unitRepository, new MockAirfieldRepository(), false, null, null, "TEST");

            var player = new ITransmission.Player("dolt", 1, 2);
            var awacs = new ITransmission.Awacs("Overlord");
            var qualifier = new ITransmission.Aircraft(null, null);
            var transmission = new BogeyDopeTransmission(null, ITransmission.Intents.BogeyDope, player, awacs, qualifier);

            const string expectedResponse = "dolt 1 2, overlord, bra, 3 5 3, 29, five thousand, DRAG NORTH, fast, Fishbed";
            var actualResponse = await controller.ProcessTransmission(transmission);

            Assert.AreEqual(expectedResponse, actualResponse.ToSpeech());
        }

        [TestMethod]
        public async Task WhenContactHasMultipleComments_ThenReturnsCorrectResponse()
        {
            var playerRepository = new MockPlayerRepository
            {
                FindByCallSignResult = new Unit
                {
                    Location = new Point(41, 37),
                    Name = "F-15C"
                }
            };

            var unitRepository = new MockUnitRepository
            {
                FindHostileAircraftResult = new List<Unit>
                {
                    new()
                    {
                        Location = new Point(41.5, 37),
                        Name = "MiG-21Bis",
                        Altitude = 15000,
                        Speed = 400
                    }
                }
            };

            var controller = new Awacs.AwacsResponder(playerRepository, unitRepository, new MockAirfieldRepository(), false, null, null, "TEST");

            var player = new ITransmission.Player("dolt", 1, 2);
            var awacs = new ITransmission.Awacs("Overlord");
            var qualifier = new ITransmission.Aircraft(null, null);
            var transmission = new BogeyDopeTransmission(null, ITransmission.Intents.BogeyDope, player, awacs, qualifier);

            const string expectedResponse = "dolt 1 2, overlord, bra, 3 5 3, 29, forty-nine thousand, DRAG NORTH, high, fast, Fishbed";
            var actualResponse = await controller.ProcessTransmission(transmission);

            Assert.AreEqual(expectedResponse, actualResponse.ToSpeech());
        }

        #endregion

        #region Multiple Contact tests

        [TestMethod]
        public async Task WhenTwoEnemyContactsAndOfSameType_ThenReturnsCorrectResponse()
        {
            var playerRepository = new MockPlayerRepository
            {
                FindByCallSignResult = new Unit
                {
                    Location = new Point(41, 37),
                    Name = "F-15C"
                }
            };

            var unitRepository = new MockUnitRepository
            {
                FindHostileAircraftResult = new List<Unit>
                {
                    new()
                    {
                        Id = "1",
                        Location = new Point(41.5, 37),
                        Name = "MiG-21Bis",
                        Altitude = 1450
                    },
                    new()
                    {
                        Id = "2",
                        Location = new Point(41.50001, 37),
                        Name = "MiG-21Bis",
                        Altitude = 1450
                    }
                }
            };

            var controller = new Awacs.AwacsResponder(playerRepository, unitRepository, new MockAirfieldRepository(), false, null, null, "TEST");

            var player = new ITransmission.Player("dolt", 1, 2);
            var awacs = new ITransmission.Awacs("Overlord");
            var qualifier = new ITransmission.Aircraft(null, null);
            var transmission = new BogeyDopeTransmission(null, ITransmission.Intents.BogeyDope, player, awacs, qualifier);

            const string expectedResponse = "dolt 1 2, overlord, bra, 3 5 3, 29, five thousand, DRAG NORTH, 2-ship Fishbed";
            var actualResponse = await controller.ProcessTransmission(transmission);

            Assert.AreEqual(expectedResponse, actualResponse.ToSpeech());
        }

        [TestMethod]
        public async Task WhenThreeEnemyContactsAndOfSameType_ThenReturnsCorrectResponse()
        {
            var playerRepository = new MockPlayerRepository
            {
                FindByCallSignResult = new Unit
                {
                    Location = new Point(41, 37),
                    Name = "F-15C"
                }
            };

            var unitRepository = new MockUnitRepository
            {
                FindHostileAircraftResult = new List<Unit>
                {
                    new()
                    {
                        Id = "1",
                        Location = new Point(41.5, 37),
                        Name = "MiG-21Bis",
                        Altitude = 1450
                    },
                    new()
                    {
                        Id = "2",
                        Location = new Point(41.50001, 37),
                        Name = "MiG-21Bis",
                        Altitude = 1450
                    },
                    new()
                    {
                        Id = "3",
                        Location = new Point(41.50002, 37),
                        Name = "MiG-21Bis",
                        Altitude = 1450
                    }
                }
            };

            var controller = new Awacs.AwacsResponder(playerRepository, unitRepository, new MockAirfieldRepository(), false, null, null, "TEST");

            var player = new ITransmission.Player("dolt", 1, 2);
            var awacs = new ITransmission.Awacs("Overlord");
            var qualifier = new ITransmission.Aircraft(null, null);
            var transmission = new BogeyDopeTransmission(null, ITransmission.Intents.BogeyDope, player, awacs, qualifier);

            const string expectedResponse = "dolt 1 2, overlord, bra, 3 5 3, 29, five thousand, DRAG NORTH, 3-ship Fishbed";
            var actualResponse = await controller.ProcessTransmission(transmission);

            Assert.AreEqual(expectedResponse, actualResponse.ToSpeech());
        }

        [TestMethod]
        public async Task WhenTwoEnemyContactsAndOfDifferentType_ThenReturnsCorrectResponse()
        {
            var playerRepository = new MockPlayerRepository
            {
                FindByCallSignResult = new Unit
                {
                    Location = new Point(41, 37),
                    Name = "F-15C"
                }
            };

            var unitRepository = new MockUnitRepository
            {
                FindHostileAircraftResult = new List<Unit>
                {
                    new()
                    {
                        Id = "1",
                        Location = new Point(41.5, 37),
                        Name = "MiG-21Bis",
                        Altitude = 1450
                    },
                    new()
                    {
                        Id = "2",
                        Location = new Point(41.50001, 37),
                        Name = "MiG-19P",
                        Altitude = 1450
                    }
                }
            };

            var controller = new Awacs.AwacsResponder(playerRepository, unitRepository, new MockAirfieldRepository(), false, null, null, "TEST");

            var player = new ITransmission.Player("dolt", 1, 2);
            var awacs = new ITransmission.Awacs("Overlord");
            var qualifier = new ITransmission.Aircraft(null, null);
            var transmission = new BogeyDopeTransmission(null, ITransmission.Intents.BogeyDope, player, awacs, qualifier);

            const string expectedResponse = "dolt 1 2, overlord, bra, 3 5 3, 29, five thousand, DRAG NORTH, Farmer, Fishbed";
            var actualResponse = await controller.ProcessTransmission(transmission);

            Assert.AreEqual(expectedResponse, actualResponse.ToSpeech());
        }

        [TestMethod]
        public async Task WhenThreeEnemyContactsOnePairOneSingleton_ThenReturnsCorrectResponse()
        {
            var playerRepository = new MockPlayerRepository
            {
                FindByCallSignResult = new Unit
                {
                    Location = new Point(41, 37),
                    Name = "F-15C"
                }
            };

            var unitRepository = new MockUnitRepository
            {
                FindHostileAircraftResult = new List<Unit>
                {
                    new()
                    {
                        Id = "1",
                        Location = new Point(41.5, 37),
                        Name = "MiG-21Bis",
                        Altitude = 1450
                    },
                    new()
                    {
                        Id = "2",
                        Location = new Point(41.50001, 37),
                        Name = "MiG-21Bis",
                        Altitude = 1450
                    },
                    new()
                    {
                        Id = "3",
                        Location = new Point(41.50002, 37),
                        Name = "MiG-19P",
                        Altitude = 1450
                    },
                }
            };

            var controller = new Awacs.AwacsResponder(playerRepository, unitRepository, new MockAirfieldRepository(), false, null, null, "TEST");

            var player = new ITransmission.Player("dolt", 1, 2);
            var awacs = new ITransmission.Awacs("Overlord");
            var qualifier = new ITransmission.Aircraft(null, null);
            var transmission = new BogeyDopeTransmission(null, ITransmission.Intents.BogeyDope, player, awacs, qualifier);

            const string expectedResponse = "dolt 1 2, overlord, bra, 3 5 3, 29, five thousand, DRAG NORTH, Farmer, 2-ship Fishbed";
            var actualResponse = await controller.ProcessTransmission(transmission);

            Assert.AreEqual(expectedResponse, actualResponse.ToSpeech());
        }

        #endregion

        [TestMethod]
        public async Task WhenContactIsAtZeroRoundedAltitude_ThenReturnsCorrectResponse()
        {
            var playerRepository = new MockPlayerRepository
            {
                FindByCallSignResult = new Unit
                {
                    Location = new Point(41, 37),
                    Name = "F-15C"
                }
            };

            var unitRepository = new MockUnitRepository
            {
                FindHostileAircraftResult = new List<Unit>
                {
                    new()
                    {
                        Location = new Point(41.5, 37),
                        Name = "MiG-21Bis",
                        Altitude = 100
                    }
                }
            };

            var controller = new Awacs.AwacsResponder(playerRepository, unitRepository, new MockAirfieldRepository(), false, null, null, "TEST");

            var player = new ITransmission.Player("dolt", 1, 2);
            var awacs = new ITransmission.Awacs("Overlord");
            var qualifier = new ITransmission.Aircraft(null, null);
            var transmission = new BogeyDopeTransmission(null, ITransmission.Intents.BogeyDope, player, awacs, qualifier);

            const string expectedResponse = "dolt 1 2, overlord, bra, 3 5 3, 29, five hundred, DRAG NORTH, Fishbed";
            var actualResponse = await controller.ProcessTransmission(transmission);

            Assert.AreEqual(expectedResponse, actualResponse.ToSpeech());
        }

    }
}
