﻿// OverlordBot is an AWACS/ATC bot for DCS World
// Copyright (C) 2022 Jeffrey Jones
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY

using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using RurouniJones.OverlordBot.Cognitive.LanguageUnderstanding.Transmissions;
using RurouniJones.OverlordBot.Core.Tests.Mocks;

namespace RurouniJones.OverlordBot.Core.Tests.AwacsResponder
{
    /// <summary>
    /// The transmitter is mistakenly transmitting to ATC on an AWACS frequency.
    /// </summary>
    [TestClass]
    public class AtcReceiverRecognitionTests
    {
        [TestMethod]
        public async Task WhenAPlayerCallsAtcWithFullyRecognizableCallsign_ThenReturnsNoResponse()
        {
            var controller = new Awacs.AwacsResponder(new MockPlayerRepository(), new MockUnitRepository(), new MockAirfieldRepository(), false, null, null, "TEST");
            var player = new ITransmission.Player("Dolt", 1, 1);
            var atc = new ITransmission.Airfield(null, null);
            var transmission = new BasicTransmission(null, ITransmission.Intents.RadioCheck, player, atc);

            var reply = await controller.ProcessTransmission(transmission);

            Assert.IsNull(reply.ToSpeech());
        }

        [TestMethod]
        public async Task WhenAPlayerCallsAtcWithPartiallyRecognizableCallsign_ThenReturnsNoResponse()
        {
            var controller = new Awacs.AwacsResponder(new MockPlayerRepository(), new MockUnitRepository(), new MockAirfieldRepository(), false, null, null, "TEST");
            var player = new ITransmission.Player("Dolt", -1, -1);
            var atc = new ITransmission.Airfield(null, null);
            var transmission = new BasicTransmission(null, ITransmission.Intents.RadioCheck, player, atc);

            var reply = await controller.ProcessTransmission(transmission);

            Assert.IsNull(reply.ToSpeech());
        }

        [TestMethod]
        public async Task WhenAPlayerCallsAtcWithUnrecognizableCallsign_ThenReturnsNoResponse()
        {
            var controller = new Awacs.AwacsResponder(new MockPlayerRepository(), new MockUnitRepository(), new MockAirfieldRepository(), false, null, null, "TEST");
            var player = new ITransmission.Player(null, -1, -1);
            var atc = new ITransmission.Airfield(null, null);
            var transmission = new BasicTransmission(null, ITransmission.Intents.RadioCheck, player, atc);

            var reply = await controller.ProcessTransmission(transmission);

            Assert.IsNull(reply.ToSpeech());
        }

        [TestMethod]
        public async Task WhenAPlayerCallsAtcWithNoTransmitter_ThenReturnsNoResponse()
        {
            var controller = new Awacs.AwacsResponder(new MockPlayerRepository(), new MockUnitRepository(), new MockAirfieldRepository(), false, null, null, "TEST");
            var atc = new ITransmission.Airfield(null, null);
            var transmission = new BasicTransmission(null, ITransmission.Intents.RadioCheck, null, atc);
            
            var reply = await controller.ProcessTransmission(transmission);

            Assert.IsNull(reply.ToSpeech());
        }
    }
}
