﻿// OverlordBot is an AWACS/ATC bot for DCS World
// Copyright (C) 2022 Jeffrey Jones
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY

using Google.Protobuf.Collections;
using Google.Protobuf.WellKnownTypes;
using Grpc.Core;
using Microsoft.Extensions.Logging;
using RurouniJones.Dcs.Grpc.V0.Coalition;
using RurouniJones.Dcs.Grpc.V0.Common;
using RurouniJones.Dcs.Grpc.V0.Group;
using RurouniJones.Dcs.Grpc.V0.Mission;

namespace RurouniJones.TextalordBot.Console
{
    public class RadioMenuManager
    {
        private readonly ILogger<RadioMenuManager> _logger;
        private readonly MissionService.MissionServiceClient _missionClient;
        private readonly CoalitionService.CoalitionServiceClient _coalitionClient;
        private readonly GroupService.GroupServiceClient _groupClient;

        public RadioMenuManager(ILogger<RadioMenuManager> logger, DcsServer server)
        {
            _logger = logger;
            _missionClient = new(server.Channel);
            _coalitionClient = new(server.Channel);
            _groupClient = new(server.Channel);
        }

        public async Task<List<Group>> GetFlyableGroups(CancellationToken stoppingToken)
        {
            var fixedWing = await _coalitionClient.GetGroupsAsync(new GetGroupsRequest() { Category = GroupCategory.Airplane});
            var rotary = await _coalitionClient.GetGroupsAsync(new GetGroupsRequest() { Category = GroupCategory.Helicopter});

            var returnValue = fixedWing.Groups.ToList();
            returnValue.AddRange(rotary.Groups.ToList());
            return returnValue;
        }

        public async Task AddMenuToAllPlayerGroups(CancellationToken stoppingToken)
        {
            try {
                _logger.LogInformation($"Adding OverlordBot menu to all flying groups with an active player");
                List<Task> tasks = new();
                foreach (var group in await GetFlyableGroups(stoppingToken))
                {
                    try { 
                        var unitResponse = await _groupClient.GetUnitsAsync(new GetUnitsRequest() { GroupName = group.Name, Active = true });

                        if (unitResponse.Units.Any(u => !string.IsNullOrEmpty(u.PlayerName)))
                        {
                            tasks.Add(CreateOverlordBotMenu(group.Name, stoppingToken));
                        }
                    } catch (RpcException ex)
                    {
                        if (ex.StatusCode == StatusCode.NotFound)
                        {
                            _logger.LogWarning($"{group.Name} not found. Skipping radio menu creation.");
                            continue;
                        } else
                        {
                            _logger.LogWarning(ex.Message);
                            continue;
                        }
                    }
                }
                await Task.WhenAll(tasks);
                _logger.LogInformation($"Added OverlordBot menu to all flying groups with an active player");
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message, ex);
                return;
            }
        }

        public async Task RemoveOverlordBotMenuFromAllPlayerGroups()
        {
            try
            {
                _logger.LogInformation($"Removing OverlordBot menu from all flying groups");
                List<Task> tasks = new();
                foreach (var group in await GetFlyableGroups(new CancellationToken()))
                {
                    try
                    {
                        var unitResponse = await _groupClient.GetUnitsAsync(new GetUnitsRequest() { GroupName = group.Name});
                        tasks.Add(RemoveOverlordBotMenu(group.Name));
                    }
                    catch (RpcException ex)
                    {
                        if (ex.StatusCode == StatusCode.NotFound)
                        {
                            _logger.LogWarning($"{group.Name} not found. Skipping radio menu deletion.");
                            continue;
                        }
                        else
                        {
                            _logger.LogWarning(ex.Message);
                            continue;
                        }
                    }
                }
                await Task.WhenAll(tasks);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message, ex);
                return;
            }
        }

        public async Task CreateOverlordBotMenu(string groupName, CancellationToken stoppingToken)
        {
            _logger.LogInformation($"Creating OverlordBot menu for {groupName}");
            var response = await CreateTopMenu(groupName, stoppingToken);
            try { 
                await CreateBogeyDopeCommand(groupName, response.Path, stoppingToken);
                await CreateWarningRadiusCommands(groupName, response.Path, stoppingToken);
            } catch (RpcException ex)
            {
                if (ex.StatusCode == StatusCode.NotFound)
                {
                    _logger.LogWarning($"{groupName} not found. Skipping radio menu creation");
                }
            }
        }
        public async Task RemoveOverlordBotMenu(string groupName)
        {
            _logger.LogInformation($"Removing OverlordBot menu from {groupName}");
            await _missionClient.RemoveGroupCommandItemAsync(new RemoveGroupCommandItemRequest()
            {
                GroupName = groupName,
                Path = { "OverlordBot" }
            });
        }

        public async Task RemoveOverlordBotMenu(string groupName, CancellationToken stoppingToken)
        {
            _logger.LogDebug($"Removing OverlordBot menu from {groupName}");
            await _missionClient.RemoveGroupCommandItemAsync(new RemoveGroupCommandItemRequest()
            {
                GroupName = groupName,
                Path = { "OverlordBot" }
            }, cancellationToken: stoppingToken);
        }


        private async Task<AddGroupCommandSubMenuResponse> CreateTopMenu(string groupName, CancellationToken stoppingToken)
        {
            _logger.LogDebug($"Removing existing top menu");
            await RemoveOverlordBotMenu(groupName, stoppingToken);

            _logger.LogDebug($"Adding new top menu");
            return await _missionClient.AddGroupCommandSubMenuAsync(new AddGroupCommandSubMenuRequest()
            {
                GroupName = groupName,
                Name = "OverlordBot"
            }, cancellationToken: stoppingToken);
        }

        private async Task CreateBogeyDopeCommand(string groupName, RepeatedField<string> path, CancellationToken stoppingToken)
        {
            _logger.LogDebug($"Adding new bogey dope command");
            await _missionClient.AddGroupCommandAsync(new AddGroupCommandRequest()
            {
                GroupName = groupName,
                Name = "Bogey Dope",
                Path = { path },
                Details = new Struct
                {
                    Fields =
                    {
                        ["commandName"] = Value.ForString(Worker.BogeyDope)
                    }
                }
            }, cancellationToken: stoppingToken);
        }

        private async Task CreateWarningRadiusCommands(string groupName, RepeatedField<string> path, CancellationToken stoppingToken)
        {
            _logger.LogDebug($"Adding new warning radius commands");
            await CreateMergedWarningCommand(groupName, path, stoppingToken);
            await CreateWarningRadiusCommand(groupName, path, 10, stoppingToken);
            await CreateWarningRadiusCommand(groupName, path, 15, stoppingToken);
            await CreateWarningRadiusCommand(groupName, path, 20, stoppingToken);
            await CreateWarningRadiusCommand(groupName, path, 30, stoppingToken);
            await CreateWarningRadiusCommand(groupName, path, 40, stoppingToken);
            await CreateCancelWarningRadiusCommand(groupName, path, stoppingToken);
        }

        private async Task CreateMergedWarningCommand(string groupName, RepeatedField<string> path, CancellationToken stoppingToken)
        {
            _logger.LogDebug($"Adding new merged warning command");
            await _missionClient.AddGroupCommandAsync(new AddGroupCommandRequest()
            {
                GroupName = groupName,
                Name = $"Tripwire Merged Only",
                Path = { path },
                Details = new Struct
                {
                    Fields =
                    {
                        ["commandName"] = Value.ForString(Worker.SetTripWire),
                        ["distance"] = Value.ForNumber(1)
                    }
                }
            }, cancellationToken: stoppingToken);
        }

        private async Task CreateWarningRadiusCommand(string groupName, RepeatedField<string> path, int distance, CancellationToken stoppingToken)
        {
            _logger.LogDebug($"Adding new warning radius command. Distance {distance}");
            await _missionClient.AddGroupCommandAsync(new AddGroupCommandRequest()
            {
                GroupName = groupName,
                Name = $"Tripwire {distance}",
                Path = { path },
                Details = new Struct
                {
                    Fields =
                    {
                        ["commandName"] = Value.ForString(Worker.SetTripWire),
                        ["distance"] = Value.ForNumber(distance)
                    }
                }
            }, cancellationToken: stoppingToken);
        }

        private async Task CreateCancelWarningRadiusCommand(string groupName, RepeatedField<string> path, CancellationToken stoppingToken)
        {
            _logger.LogDebug($"Adding new cancel warning radius command");
            await _missionClient.AddGroupCommandAsync(new AddGroupCommandRequest()
            {
                GroupName = groupName,
                Name = $"Cancel Tripwire",
                Path = { path },
                Details = new Struct
                {
                    Fields =
                    {
                        ["commandName"] = Value.ForString(Worker.CancelTripWire)
                    }
                }
            }, cancellationToken: stoppingToken);
        }
    }
}
