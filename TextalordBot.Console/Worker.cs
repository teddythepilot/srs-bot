﻿/* 
Custodian is a DCS server administration tool for Discord
Copyright (C) 2022 Jeffrey Jones

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

using Grpc.Core;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using RurouniJones.Dcs.Grpc.V0.Mission;
using RurouniJones.Dcs.Grpc.V0.Group;
using RurouniJones.Dcs.Grpc.V0.Trigger;
using RurouniJones.OverlordBot.Datastore.Models;
using Geo.Geometries;
using RurouniJones.OverlordBot.GrpcDatastore;
using System.Collections.Concurrent;
using RurouniJones.OverlordBot.Cognitive.LanguageUnderstanding.Transmissions;
using static RurouniJones.OverlordBot.Cognitive.LanguageUnderstanding.Transmissions.ITransmission;
using RurouniJones.OverlordBot.Core.Awacs.IntentHandlers;
using RurouniJones.OverlordBot.Core.Responders;
using RurouniJones.Dcs.Grpc.V0.Coalition;

namespace RurouniJones.TextalordBot.Console
{
    public class Worker : BackgroundService
    {
        private readonly ILogger<Worker> _logger;
        private readonly List<Task> _tasks;

        private readonly MissionService.MissionServiceClient _missionClient;
        private readonly GroupService.GroupServiceClient _groupClient;
        private readonly TriggerService.TriggerServiceClient _triggerClient;
        private readonly CoalitionService.CoalitionServiceClient _coalitionClient;

        private readonly RadioMenuManager _radioMenuManager;

        public const string BogeyDope = "overlordBotBogeyDope";
        public const string SetTripWire = "overlordBotSetTripwire";
        public const string CancelTripWire = "overlordBotCancelTripwire";

        private readonly DataSource _dataSource;
        private readonly UnitRepository _unitRepository;
        private readonly PlayerRepository _playerRepository;
        private readonly AirfieldRepository _airfieldRepository;

        private readonly Monitor _monitor;

        private readonly Dictionary<string, DateTime> _lastTransmission;

        public Worker(ILogger<Worker> logger, DcsServer server, RadioMenuManager radioMenuManager, MonitorFactory monitorFactory)
        {
            _logger = logger;
            _tasks = new();

            _missionClient = new(server.Channel);
            _groupClient = new(server.Channel);
            _triggerClient = new(server.Channel);
            _coalitionClient = new(server.Channel);

            _radioMenuManager = radioMenuManager;

            _dataSource = new("localhost", 50051, new ConcurrentQueue<StreamUnitsResponse>());
            _unitRepository = new UnitRepository(_dataSource);
            _playerRepository = new PlayerRepository(_dataSource);
            _airfieldRepository = new AirfieldRepository(_dataSource);
            _lastTransmission = new();

            _monitor = monitorFactory.CreateMonitor(_playerRepository, _unitRepository, _coalitionClient, _triggerClient);
        }

        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            while (!stoppingToken.IsCancellationRequested)
            {
                try
                {
                    if (OperatingSystem.IsWindows() && Environment.UserInteractive) 
                        System.Console.Title = $"TextalordBot Version {typeof(Program).Assembly.GetName().Version}";

                    _tasks.Add(_dataSource.StreamUnitUpdatesAsync(stoppingToken));
                    _tasks.Add(_radioMenuManager.AddMenuToAllPlayerGroups(stoppingToken));
                    _tasks.Add(MonitorEvents(stoppingToken));
                    _tasks.Add(_monitor.StartMonitoring(stoppingToken));
                    await Task.WhenAll(_tasks);
                }
                catch (OperationCanceledException)
                {
                    break;
                }
            }
            await _radioMenuManager.RemoveOverlordBotMenuFromAllPlayerGroups();
        }

        private async Task ProcessBogeyDopeRequest(StreamEventsResponse.Types.GroupCommandEvent command, CancellationToken stoppingToken)
        {
            _logger.LogInformation($"Bogey Dope request received from {command.Group.Name}");

            var unitResponse = await _groupClient.GetUnitsAsync(new GetUnitsRequest()
            {
                GroupName = command.Group.Name
            }, cancellationToken: stoppingToken);
            var transmitter = unitResponse.Units[0];

            if(_lastTransmission.ContainsKey(transmitter.Id.ToString()))
            {
                DateTime previousTransmission = _lastTransmission[transmitter.Id.ToString()];

                if ((DateTime.UtcNow - previousTransmission).TotalSeconds < 30)
                {
                    int waitTime = 60 - (int)(DateTime.UtcNow - previousTransmission).TotalSeconds;

                    await _triggerClient.OutTextForGroupAsync(new OutTextForGroupRequest()
                    {
                        GroupId = command.Group.Id,
                        Text = $"Wait {waitTime} seconds",
                        ClearView = false,
                        DisplayTime = 15
                    }, cancellationToken: stoppingToken);
                    return;
                }
            }

            _lastTransmission[transmitter.Id.ToString()] = DateTime.UtcNow;

            var newUnit = new Unit
            {
                Coalition = (int)transmitter.Coalition - 1,
                Id = transmitter.Id.ToString(),
                Location = new Point(transmitter.Position.Lat, transmitter.Position.Lon, transmitter.Position.Alt),
                Altitude = transmitter.Position.Alt,
                Heading = (int)transmitter.Orientation.Heading,
                Group = transmitter.Group.Name,
                Name = transmitter.Type,
                Pilot = transmitter.PlayerName,
                Callsign = transmitter.Callsign,
                Speed = (int)transmitter.Velocity.Speed,
                MilStd2525d = new MilStd2525d((int)transmitter.Coalition - 1, OverlordBot.Encyclopedia.MilStd2525d.GetUnitByDcsCode(transmitter.Type)?.MilStdCode)
            };

            var reply = await new BogeyDopeHandler(_unitRepository).Process(
                new BogeyDopeTransmission("", Intents.BogeyDope, new Player("Caller", 0, 0), null, null),
                newUnit,
                null,
                new Reply(),
                "Server",
                false);

            string replyText = string.IsNullOrEmpty(reply.Message) ? reply.Details.ToText() : reply.Message;
            await _triggerClient.OutTextForGroupAsync(new OutTextForGroupRequest()
            {
                GroupId = command.Group.Id,
                Text = $"{replyText}",
                ClearView = false,
                DisplayTime = 15
            }, cancellationToken: stoppingToken);
        }

        private async Task ProcessTripwireCreationRequest(StreamEventsResponse.Types.GroupCommandEvent command, CancellationToken stoppingToken)
        {
            _logger.LogInformation($"Tripwire received from {command.Group.Name}");

            var unitResponse = await _groupClient.GetUnitsAsync(new GetUnitsRequest()
            {
                GroupName = command.Group.Name
            }, cancellationToken: stoppingToken);
            var transmitter = unitResponse.Units[0];

            var transmitterUnit = new Unit
            {
                Coalition = (int)transmitter.Coalition - 1,
                Id = transmitter.Id.ToString(),
                Location = new Point(transmitter.Position.Lat, transmitter.Position.Lon, transmitter.Position.Alt),
                Altitude = transmitter.Position.Alt,
                Heading = (int)transmitter.Orientation.Heading,
                Group = transmitter.Group.Name,
                Name = transmitter.Type,
                Pilot = transmitter.PlayerName,
                Callsign = transmitter.Callsign,
                Speed = (int)transmitter.Velocity.Speed,
                MilStd2525d = new MilStd2525d((int)transmitter.Coalition - 1, OverlordBot.Encyclopedia.MilStd2525d.GetUnitByDcsCode(transmitter.Type)?.MilStdCode)
            };

            var distance = (int) command.Details.Fields["distance"].NumberValue;
            var measurement = BogeyDopeHandler.MetricAirframes.Contains(transmitterUnit.Name) ? "kilometers" : "nautical miles";
            string message;

            _monitor.SetWarningRadius(transmitterUnit, distance);

            if ((distance > 5 && measurement == "nautical miles") || (distance > 10 && measurement == "kilometers"))
            {
                message = $"Tripwire set for {distance} {measurement}";
            }
            else
            {
                message = "Tripwire set for merge calls only";
            }

            await _triggerClient.OutTextForGroupAsync(new OutTextForGroupRequest()
            {
                GroupId = command.Group.Id,
                Text = message,
                ClearView = false,
                DisplayTime = 15
            }, cancellationToken: stoppingToken);
        }

        private async Task ProcessTripwireCancellationRequest(StreamEventsResponse.Types.GroupCommandEvent command, CancellationToken stoppingToken)
        {
            var unitResponse = await _groupClient.GetUnitsAsync(new GetUnitsRequest()
            {
                GroupName = command.Group.Name
            }, cancellationToken: stoppingToken);
            var transmitter = unitResponse.Units[0];

            var transmitterUnit = new Unit
            {
                Coalition = (int)transmitter.Coalition - 1,
                Id = transmitter.Id.ToString(),
                Location = new Point(transmitter.Position.Lat, transmitter.Position.Lon, transmitter.Position.Alt),
                Altitude = transmitter.Position.Alt,
                Heading = (int)transmitter.Orientation.Heading,
                Group = transmitter.Group.Name,
                Name = transmitter.Type,
                Pilot = transmitter.PlayerName,
                Callsign = transmitter.Callsign,
                Speed = (int)transmitter.Velocity.Speed,
                MilStd2525d = new MilStd2525d((int)transmitter.Coalition - 1, OverlordBot.Encyclopedia.MilStd2525d.GetUnitByDcsCode(transmitter.Type)?.MilStdCode)
            };

            await _triggerClient.OutTextForGroupAsync(new OutTextForGroupRequest()
            {
                GroupId = command.Group.Id,
                Text = $"Tripwire cancelled",
                ClearView = false,
                DisplayTime = 15
            }, cancellationToken: stoppingToken);
        }

        private async Task MonitorEvents(CancellationToken stoppingToken)
        {
            while (!stoppingToken.IsCancellationRequested)
            {
                try
                {
                    _logger.LogInformation($"Connecting to DCS-gRPC event stream");
                    var events = _missionClient.StreamEvents(new StreamEventsRequest() { }, cancellationToken: stoppingToken);

                    while (await events.ResponseStream.MoveNext(stoppingToken))
                    {
                        var dcsEvent = events.ResponseStream.Current;
                        switch (dcsEvent.EventCase)
                        {
                            case StreamEventsResponse.EventOneofCase.Birth:
                                var birthGroupName = dcsEvent.Birth.Initiator?.Unit?.Group.Name;
                                var birthPlayerName = dcsEvent.Birth.Initiator?.Unit?.PlayerName;
                                if (!string.IsNullOrEmpty(birthGroupName) && !string.IsNullOrEmpty(birthPlayerName))
                                {
                                    _logger.LogInformation($"Birth event for Player {birthPlayerName}. Group {birthGroupName}");
                                    await _radioMenuManager.CreateOverlordBotMenu(birthGroupName, stoppingToken);
                                }
                                break;
                            case StreamEventsResponse.EventOneofCase.GroupCommand:
                                var command = dcsEvent.GroupCommand;
                                var groupCommandName = command.Details.Fields["commandName"]?.StringValue;
                                switch (groupCommandName)
                                {
                                    case null:
                                        break;
                                    case BogeyDope:
                                        await ProcessBogeyDopeRequest(command, stoppingToken);
                                        break;
                                    case SetTripWire:
                                        await ProcessTripwireCreationRequest(command, stoppingToken);
                                        break;
                                    case CancelTripWire:
                                        await ProcessTripwireCancellationRequest(command, stoppingToken);
                                        break;
                                    default:
                                    break;
                                }
                                break;
                            default:
                                continue;
                        }
                    }
                }
                catch (TaskCanceledException)
                {
                    _logger.LogInformation($"Stopped DCS-gRPC event stream");
                    break;
                }
                catch (RpcException)
                {
                    _logger.LogInformation($"Disconnected from DCS-gRPC event stream");
                    await Task.Delay(TimeSpan.FromSeconds(10), stoppingToken);
                    continue;
                }
                catch (Exception ex)
                {
                    _logger.LogError($"Exception processing stream", ex);
                    continue;
                }
            }
        }
    }
}
