﻿// OverlordBot is an AWACS/ATC bot for DCS World
// Copyright (C) 2022 Jeffrey Jones
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY

using System;
using System.Text;

namespace RurouniJones.OverlordBot.Discord
{
    public class RequestReplyTransmissionLog : TransmissionLog
    {
        // Class Specific
        public string Response { get; set; }
        public string TransmissionDetails { get; set; }
        public Exception Exception { get; set; }
        public long ProcessingTime { get; set; }
        public string Notes { get; set; }

        public override string ToDiscordLog()
        {
            PlayersOnFrequency.Sort();
            var sb = new StringBuilder("Transmission Detected");
                AddHeader(sb)
                    .Append(TransmissionDetails)
                    .AppendLine($"Response: {Response}")
                    .AppendLine($"Processing Time: {ProcessingTime}ms");
            if (Notes != null)
            {
                sb.AppendLine($"Notes: {Notes}");
            }
            if (Exception != null)
            {
                sb.AppendLine($"Exception: {Exception.Message}");
            }
            sb.AppendLine("```");

            return sb.ToString();
        }
    }
}