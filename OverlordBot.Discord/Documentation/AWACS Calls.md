﻿**AWACS Calls**
* Radio Check                `Overlord, CALLSIGN N-N, radio /comm / comms check` (**Don't use `Mic check` / `Mike check`**)
* Bogey Dope                 `Overlord, CALLSIGN N-N, bogey dope`
* Bearing to an airfield     `Overlord, CALLSIGN N-N, bearing to / vector to / heading to / where is / AIRFIELD_NAME`
  * You can also use `nearest airfield` for nearest friendly airfield
  * You can use the the ICAO,  e.g. `Oscar Kilo November Uniform ` for `OKNU`  ).
  * `home plate` is not supported.
* Position of Friendly       `Overlord, CALLSIGN N-N, bearing to / vector to / heading to / where is / CALLSIGN N-N`
* Proactive warning          `Overlord, CALLSIGN N-N, set tripwire NN miles` (Recommend 40 miles or less) **Include "miles" in your request. Don't just say "30"**
* Cancel warning             `Overlord, CALLSIGN N-N, cancel tripwire`
* Picture                    *Not Supported*
* Declare                    Requires ED to fix / implement API. Please comment with your support on @ <https://forums.eagle.ru/topic/251461-request-for-scripting-apis-to-be-implemented-to-enhance-overlordbot-functionality/>