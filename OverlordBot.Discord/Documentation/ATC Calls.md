﻿**ATC Calls** (Work-in-progress, limited airfields and servers)
* Ready to Taxi        `AIRFIELD_NAME ground, CALLSIGN N-N, ready to taxi`. Available on most Caucasus airfields.
* Inbound for Landing  `AIRFIELD_NAME approach, CALLSIGN N-N, inbound` . Can also call `tower`. Currently only on  `Anapa`, `Kutaisi`, `Maykop` and `Krymsk`
* Cancel Inbound       `AIRFIELD_NAME approach, CALLSIGN N-N, cancel inbound`. Can also call `tower`. Currently only on `Anapa` and `Kutaisi`, `Maykop` and `Krymsk`

** Features and limitations**
* The bot will provide runway entry, crossing and take-off instructions based on status of other players who have called into the bot for ATC instructions.
* The bot will _not_ see players who have not called into the bot for ATC.
* The bot will _not_ see AI.
* The bot will _not_ give inbound aircraft instructions to deconflict or maintain inbound spacing.