// OverlordBot is an AWACS/ATC bot for DCS World
// Copyright (C) 2022 Jeffrey Jones
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY

using System;
using System.IO;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using RurouniJones.OverlordBot.Core;

namespace RurouniJones.OverlordBot.Console
{
    public class Worker : BackgroundService
    {
        private readonly ILogger<Worker> _logger;
        private readonly Bot _bot;

        public Worker(ILogger<Worker> logger, Bot bot)
        {
            _logger = logger;
            _bot = bot;
        }

        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            while (!stoppingToken.IsCancellationRequested)
            {
                try
                {
                    if (OperatingSystem.IsWindows() && Environment.UserInteractive) 
                        System.Console.Title = $"OverlordBot Version {typeof(Program).Assembly.GetName().Version}";
                    _bot.CreateGameServers(stoppingToken);
                    await _bot.RunAsync();
                }
                catch (FileNotFoundException ex)
                {
                    _logger.LogCritical(ex.ToString());
                    await Task.Delay(10000, stoppingToken);
                }
                catch (OperationCanceledException)
                {
                    break;
                }
            }
        }
    }
}
